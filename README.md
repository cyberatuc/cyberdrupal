# cyberdrupal
New version of cyberatuc.org using Drupal

## Installation
### Requirements

* PHP 7
* MySQL
* Apache 2 (or any other server that can support PHP, but you'll have to write your own config files)
* Node.js and NPM
    * Gulp (`npm install -g gulp`)

### Steps
1. Install the requirements listed above.
1. Run `scripts/build.sh`.
1. Configure your Apache/MySQL/PHP stack. Drupal has a guide to do this here: <https://www.drupal.org/docs/develop/local-server-setup>

When you're setting up the web server, you'll want to configure it to serve from the `web/` directory. Here's an example VirtualHost config for Apache:

```
<VirtualHost *:80>
    DocumentRoot "/opt/cyberdrupal/web"
    ServerName cyberdrupal.localhost
    ErrorLog "/var/log/apache2/cyberdrupal.localhost-error_log"
    CustomLog "/var/log/apache2/cyberdrupal.localhost-access_log" common
    <Directory "/opt/cyberdrupal/web">
        Options +Indexes
        AllowOverride All
        Order allow,deny
        Allow from all
        Require all granted
    </Directory>
</VirtualHost>
```

If you use this example config, change `/opt/cyberdrupal` to wherever you have this repository stored. You'll also need to add this line to your `/etc/hosts` file if you want to be able to access the website at http://cyberdrupal.localhost:
```
127.0.0.1 cyberdrupal.localhost
```
