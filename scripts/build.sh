#!/usr/bin/env bash

composer install
cd web/themes/custom/cyberclean
npm install
gulp
