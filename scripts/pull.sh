#!/usr/bin/env bash

git pull
scripts_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
$scripts_dir"/build.sh"
